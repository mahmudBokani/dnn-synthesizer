# README #

This script is for the Synthesizer tool in Linux.

### Required installation to run this script ###
Need to install "cifs" in order to copy the output files to the shared folder, also need to mount the shared folder locally 
and specify it in the script or create the mount folder with the same name as used in the script “mnt/8-share”.

### Before running the script, these folder and files must exist ###
“conf.txt” file, script rename the output files and the new folder according to the values in this file.
Script must be placed beside "for_dnn_synth" folder.
Script would process the lab files from "testFolder" and also save the output to this folder.

### More info about the script ###
After running this script a log file will be created, this file lists the script run step by step.
The script also does check after each step if it was processed successfully.
The script will also calculate how much time it took to synthesize each lab file, displaying these times when finishing 
each file and also list the files and their process time at the end when finishing synthesizing all the files.

### Script run steps ###
1. Create a folder according to the Config file values, if it does not exist yet.
2. Get the name of all the lab files from the testFolder.
3. Clean the wav folder for the new run.
4. Delete the old label file "kristin_test.lab" in “for_dnn_synth” folder if it exists in order to replace it with a new one.
5. Copy a label file from "testFolder" to "for_dnn_synth" directory and rename it to "kristin_test.lab".
6. Run “run_full_voice.sh” file.
7. Copy the synthesized wav files from the "wav" folder to the "testFolder" folder
8. Compress the output folder.
9. Send the compressed file to the share folder.
